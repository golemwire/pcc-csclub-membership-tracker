package main

import (
	"fmt"
	"io"
	"strconv"
	"encoding/csv"
	"time"
	"sort"
)

type memberDatabase map[string]member

type member struct {
	studentId int
	emailAddress string
	// Freshman, sophomore, etc.
	classification string
	memberSince string
	recentMember bool
	lastUpdated string
}

// Member struct, plus name.
type fullMember struct {
	name string
	member
}

func newMemberDB() *memberDatabase {
	newMember := make(memberDatabase)
	
 return &newMember
}

/*type membershipStatus uint8
const (
	membership_yes membershipStatus = iota
	membership_no
	// "Unsure" or "N/A"
	membership_NA
)
func (ms membershipStatus) String() string {
	return [...]string {
		"Yes",
	}[ms]
}*/

var (
	// Headings for the member database file
	memberDatabaseHeadings = [...]string {
	/*0*/ "NAME",
	/*1*/ "STUDENT ID",
	/*2*/ "EMAIL",
	/*3*/ "CLASSIFICATION",
	/*4*/ "MEMBER SINCE",
	      // "Yes" if this member was added the last time the database was updated
	/*5*/ "NEW MEMBER",
	/*6*/ "LAST UPDATED",
	}
	
	// Headings for the membership form file.
	// For some reason, MS Forms puts a newline after each non-built-in custom field.
	formHeadings = [...]string { // The number comments are the numbers used in (*memberDatabase).parse() after the re-slicing (see the code)
	      "ID",
	      "Start time",
	      "Completion time",
	/*0*/ "Email",
	/*1*/ "Name",
	/*2*/ "Last modified time",
	/*3*/ "What is your student ID number?\n",
	/*4*/ "What is your classification?\n",
	/*5*/ "Are you currently a club member?\n",
	/*6*/ "Do you want to become a member?\n",
	/*7*/ "What would you like the next meeting to be about?\n",
	}
	
	invalidFieldErrFmtStr  = "Error: %q is not a valid field."
	notAMemberDBErrStr     = "Are you sure this a membership database file, and not a membership form file?"
	notAFormErrStr         = "Are you sure this a membership form file, and not a membership database file?"
)

const (
	// See documentation on the time package for information about time formats.
	mSFormsDateLayout = "1/2/06 15:04:05"
)

// Read in a member database file and store it in a memberDatabase structure.
func (mdb *memberDatabase) parse(r io.Reader) bool {
	csvR := csv.NewReader(r)
	
	record, err := csvR.Read()
	if err == io.EOF {
		fmt.Print  ("Error: file is empty.\n")
		fmt.Println(notAFormErrStr)
 return false
	}
	if err != nil { panic(err) }
	
	// Perform some initial file format validation:
	if len(record) != len(memberDatabaseHeadings) {
		fmt.Printf ("Error: header length mismatch (have %d, expected %d).\n", len(record), len(memberDatabaseHeadings))
		fmt.Println(notAMemberDBErrStr)
 return false
	}
	for i, heading := range memberDatabaseHeadings {
		if record[i] != heading {
			fmt.Printf ("Error: %q is an invalid header field. %q was expected.\n", record[i], heading)
			fmt.Println(notAMemberDBErrStr)
 return false
		}
	}
	
	for {
		record, err = csvR.Read()
		if err == io.EOF {
	 break
		}
		if err != nil { panic(err) }
		if len(record) != len(memberDatabaseHeadings) {
			fmt.Printf ("Error: record length mismatch (have %d, expected %d).\n", len(record), len(memberDatabaseHeadings))
			fmt.Println(notAMemberDBErrStr)
 return false
		}
		
		parsedMember := member {
			emailAddress:    record[2],
			classification:  record[3],
			memberSince:     record[4],
			lastUpdated:     record[6],
		}
		// Parse "MEMBER SINCE" field
		parsedMember.studentId, err = strconv.Atoi(record[1])
		if err != nil { // Invalid field
			fmt.Printf(invalidFieldErrFmtStr, record[1])
			fmt.Printf("\n%s\n", notAMemberDBErrStr)
 return false
		}
		// Parse "NEW MEMBER" field
		switch record[5] {
		case "Yes":
			parsedMember.recentMember = true
		case "No":
			parsedMember.recentMember = false
		default:
			fmt.Printf(invalidFieldErrFmtStr, record[5])
			fmt.Printf("\n%s\n", notAMemberDBErrStr)
 return false
		}
		
		// Store parsed member:
		(*mdb)[record[0]] = parsedMember
	}
	
 return true
}

// Apply a form response to a membership database.
// Returns nil on failure.
// Otherwise, returns information on what the members want in their next meeting, based on the information in the form.
func (mdb *memberDatabase) applyNew(r io.Reader) (nmp *nextMeetingPrefs) {
	csvR := csv.NewReader      (r)
	nmp   = newNextMeetingPrefs()
	
	record, err := csvR.Read()
	if err == io.EOF {
		fmt.Print  ("Error: file is empty.\n")
		fmt.Println(notAFormErrStr)
 return nil
	}
	if err != nil { panic(err) }
	
	// Perform some initial file format validation:
	if len(record) != len(formHeadings) {
		fmt.Printf ("Error: header length mismatch (have %d, expected %d).\n", len(record), len(formHeadings))
		fmt.Println(notAFormErrStr)
 return nil
	}
	for i, heading := range formHeadings {
		if record[i] != heading {
			fmt.Printf ("Error: %q is an invalid header field. %q was expected.\n", record[i], heading)
			fmt.Println(notAFormErrStr)
 return nil
		}
	}
	
	for {
		record, err = csvR.Read()
		if err == io.EOF {
	 break
		}
		if err != nil { panic(err) }
		if len(record) != len(formHeadings) {
			fmt.Printf ("Error: record length mismatch (have %d, expected %d).\n", len(record), len(formHeadings))
			fmt.Println(notAFormErrStr)
 return nil
		}
		
		// Get the time the form was completed:
		var formSubmittedDate string
		if formSubmittedTime, err := time.Parse(mSFormsDateLayout, record[2]); err != nil {
			// Invalid field
			fmt.Printf(invalidFieldErrFmtStr, record[2])
			fmt.Printf("\n%s\n", notAFormErrStr)
 return nil
		} else {
			formSubmittedDate = formSubmittedTime.Format(time.DateOnly)
		}
		
		// Cut off "ID", "Start time", and "Completion time" (doesn't cut off "Last modified time", note):
		record = record[3:]
		
		name := record[1]
		
		newMember, alreadyExists := (*mdb)[name]
		
		// Process "Are you currently a club member?\n" into possibly "MEMBER SINCE" and possibly "NEW MEMBER":
		switch currentRecord := record[5]; currentRecord {
		case "Yes":
			if !alreadyExists { // They say they are already a member, but they are new to the DB.
				fmt.Printf("Note: %s claims to already be a member, but was not in the database.\n" +
				           "    Still adding, but setting “MEMBER SINCE” field to “Unknown”.\n", name)
				newMember.memberSince  = "Unknown"
				newMember.recentMember = false
			}
		case "No":      //
		case "Unsure":  // Note to non-GOphers: fallthoughs require the `fallthrough` keyword
		default: // Invalid field
			fmt.Printf(invalidFieldErrFmtStr, currentRecord)
			fmt.Printf("\n%s\n", notAFormErrStr)
 return nil
		}
		
		// Process "Do you want to become a member?\n" and possibly abort adding the member:
		switch currentRecord := record[6]; currentRecord {
		case "Yes":
			if alreadyExists { // They say they want to join, but they are already in the DB.
				fmt.Printf("Note: %s claims to want to join, but was already in the database.\n", name)
			}
		case "No":
			if alreadyExists || newMember.memberSince == "Unknown" {
				fmt.Printf("Note: %s is a member, but said that he or she does not want to become a member (instead of answering “N/A”).\n", name)
			} else {
				fmt.Printf("Note: %s does not want to become a member (and isn't one).\n" +
				           "    Skipping.\n", name)
	 continue // Abort processing
			}
		case "N/A":
			if !(alreadyExists || newMember.memberSince == "Unknown") { // If they answer "N/A", and are new, then don't add them.
			//  !alreadyExists && newMember.memberSince != "Unknown" is equivalent, by De Morgan's laws
				fmt.Printf("Note: %s isn't a member, and answered “N/A” to becoming one.\n" +
				           "    Skipping.\n", name)
	 continue // Also abort processing
			}
		default: // Invalid field
			fmt.Printf(invalidFieldErrFmtStr, currentRecord)
			fmt.Printf("\n%s\n", notAFormErrStr)
 return nil
		}
		
		// Set "MEMBER SINCE" and possibly "NEW MEMBER":
		if alreadyExists {
			newMember.recentMember = false
		} else if newMember.memberSince != "Unknown" { // New member!
			newMember.recentMember = true
			newMember.memberSince  = formSubmittedDate
			defer fmt.Printf("NEW MEMBER: %s!\n", name)
		}
		
		newMember.emailAddress = record[0]
		
		// Process "What is your student ID number?\n":
		newStudentId, err := strconv.Atoi(record[3])
		if err != nil { // If the ID number is not a number:
			fmt.Printf (invalidFieldErrFmtStr, record[3])
			fmt.Println("\nDid someone just put their name in the student ID number field!?")
			fmt.Printf ("%s\n", notAFormErrStr)
			fmt.Println("    Setting his or her ID number to -1.")
			newStudentId = -1
		} else if alreadyExists && newStudentId != newMember.studentId {
			defer fmt.Printf("WARNING: %s changed ID number from %d to %d?\n", name, newMember.studentId, newStudentId)
		}
		newMember.studentId = newStudentId
		
		// Process "What is your classification?\n" into "CLASSIFICATION":
		newMember.classification = record[4]
		
		// Process "Completion time" into "LAST UPDATED", since this member put something on the form:
		newMember.lastUpdated = formSubmittedDate
		
		// Process "What would you like the next meeting to be about?\n"
		nmp.add(record[7])
		
		// Finally, store member:
		(*mdb)[name] = newMember
	}
	
 return // Note to non-GOphers: returns nmp.
}

func (mdb *memberDatabase) export(w io.Writer) {
	csvW := csv.NewWriter(w)
	defer csvW.Flush()
	
	csvW.Write(memberDatabaseHeadings[:])
	
	var (
		sortedMemberDB = make([]fullMember, len(*mdb))
	)
	
	// Convert the *mdb map into an array
	{var i = 0; for name, member := range *mdb { // A for-range loop for a map, with a counter
		sortedMemberDB[i] = fullMember {
			name:   name,
			member: member,
		}
	i++ }}
	
	// Sort the array
	sort.Slice(sortedMemberDB, func(i, j int) bool {
	 return sortedMemberDB[i].name < sortedMemberDB[j].name
	})
	
	for _, fm := range sortedMemberDB {
		var recentMember string
		switch fm.recentMember {
		case true:   recentMember = "Yes"
		case false:  recentMember = "No"
		}
		
		csvW.Write([]string {
			// NAME:
			fm.name,
			// STUDENT ID:
			strconv.Itoa(fm.studentId),
			// EMAIL:
			fm.emailAddress,
			// CLASSIFICATION:
			fm.classification,
			// MEMBER SINCE:
			fm.memberSince,
			// NEW MEMBER:
			recentMember,
			// LAST UPDATED:
			fm.lastUpdated,
		})
	}
}

// Try to find some errors in the database
func (mdb *memberDatabase) vetExtra() {
	// LATER: consider adding some extra vetting.
}
