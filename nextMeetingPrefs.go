package main

import (
	"fmt"
	"strings"
	"sort"
)

/*const (
	nextMeetingPref_programming   = "Programming and/or computer theory"
	nextMeetingPref_netAndInfra   = "Networking and/or server infrastructure"
	nextMeetingPref_cybersecurity = "Cybersecurity"
	nextMeetingPref_architecture  = "Computer architecture"
)*/

// Default meeting preferences.
// [April 2024] These are the checkboxes at the end of the form, other than the 'Other' checkbox.
var (
	defaultNMPResponses = [...]string {
		"Programming and/or computer theory",
		"Networking and/or server infrastructure",
		"Cybersecurity",
		"Computer architecture",
	}
)

// Holds information on what the CS Club members want in the next meeting.
// The zero-value is ready to use; no initialization is needed.
type nextMeetingPrefs struct {
	// A map of default responses to the number of occurrences of the response.
	frequency_default [len(defaultNMPResponses)]int
	// A map of 'Other' responses to the number of occurrences of the response.
	frequency_other   map[string]int
	// The total amount of occurrences of all responses.
	total             int
}
func newNextMeetingPrefs() *nextMeetingPrefs {
	// Note to non-GOphers: Go initializes values to their "zero-value"; for ints, that's `0`. For maps, however, it is `nil`, so we will need to create the map with the built-in "function" `make`().
 return &nextMeetingPrefs {
		frequency_other: make(map[string]int),
	}
}

// Add a next meeting preference record from the form to the next meeting preferences
func (nmp *nextMeetingPrefs) add(record string) {
	// Split into individual responses.
	// Note that MS Forms adds an extra ';' onto the end, unless there were no responses (in which case the record is simply empty)
	// TODO: what happens if a user enters a literal ';' into their response?
	// TODO: what happens if a user enters a default meeting preference (e.g. "Programming") into the Other field?
	// TODO: what happens if the user checks the Other checkbox, then enters nothing? (This might not really be an issue.)
	// LATER: I think I may have found a mistake in the documentation of strings.Split: it seems to incorrectly describe what happens if the first parameter is an empty string.
	responses := strings.Split(record, ";")
	
	if len(responses) != 0 {
		responses = responses[:len(responses) - 1]
		
		responseLoop: for _, response := range responses {
			// Is this a default response?
			for i, defaultResponse := range defaultNMPResponses {
				if response == defaultResponse {
					nmp.frequency_default[i]++ ; nmp.total++ // If so, add it to frequency_default.
		 continue responseLoop // This is a default response; process any others.
				}
			}
			// Otherwise, this is an 'Other' response; add it to frequency_other.
			nmp.frequency_other[response]++ ; nmp.total++
		}
	}
}

func (nmp *nextMeetingPrefs) printStatistics() {
	var (
		// nmp.frequency_other sorted, as an array.
		sortedOtherPrefs = make([]struct{name string; freq int}, len(nmp.frequency_other))
	)
	
	// Convert the nmp.frequency_other map into an array
	{var i = 0; for name, freq := range nmp.frequency_other { // A for-range loop for a map, with a counter
		sortedOtherPrefs[i] = struct{name string; freq int} {
			name: name,
			freq: freq,
		}
	i++ }}
	
	// Sort the array
	sort.Slice(sortedOtherPrefs, func(i, j int) bool {
	 return sortedOtherPrefs[i].name < sortedOtherPrefs[j].name
	})
	
	if nmp.total == 0 { // Prevents a division by 0
		fmt.Println("No next meeting preferences were given.")
	} else {
		const responseFmtStr = " %3d  |    %3d%%    | %q\n"
		
		// Print the table header
		fmt.Println(" FREQ | PROPORTION | RESPONSE\n" +
		            "------+------------+----------")
		
		// Print out the default response rows
		for i, freq := range nmp.frequency_default {
			fmt.Printf(responseFmtStr,
				// How many times this response was given
				freq,
				// What percent of responses are this response
				//int(math.Round(float64(freq) * 100.0 / float64(nmp.total))),
				(freq * 1000 / nmp.total + 5) / 10,
				// The response
				defaultNMPResponses[i])
		}
		fmt.Println("- - - + - - -- - - + - - - - -")
		// Print out the 'Other' response rows
		for _, frequency := range sortedOtherPrefs {
			fmt.Printf(responseFmtStr,
				frequency.freq,
				(frequency.freq * 1000 / nmp.total + 5) / 10,
				frequency.name)
		}
		fmt.Printf("Total next meeting preference answers: %d\n", nmp.total)
	}
}
