/*
	PCC CSClub Membership Tracker
	
	Updates the PCC Computer Science club member database with a meeting attendance form, while vetting the attendance form.
	It outputs to a file named "Membership 2023-10-30.csv" (the date is an example); the intention is that multiple copies of the database are kept, for purpose of record.
	It also can create a member database new from a meeting attendance form.
	
	By Ethan A. Black, ethan@golemwire.com (email to golemwire@gmail.com)
*/
package main

import (
	"fmt"
	"time"
	"io"
	"os"
)

func main() {
	var (
		// The most recent Membership Form response stream.
		newFormCSV      io.Reader
		// The previous membership database stream.
		prevMemberDBCSV io.Reader
		// The new membership database file.
		nextMemberDBCSV *os.File
		
		memberDB = newMemberDB()
		
		justCreateMemberDB = true
		
		err error
	)
	
	/* PROCESS CMDLINE ARGUMENTS */
	switch len(os.Args) - 1 {
	case 2: // Process 2nd argument:
		justCreateMemberDB   = false
		prevMemberDBCSV, err = os.Open(os.Args[2])
		if err != nil { panic(err) }
		fallthrough
	case 1: // Process 1st argument:
		newFormCSV, err = os.Open(os.Args[1])
		if err != nil { panic(err) }
	case 0: // No arguments; provide help:
		fmt.Print (" -- HELP: --\n")
		fmt.Printf("%s <membership-form-response> [prev-DB]\n" +
		           "\n", os.Args[0])
		fmt.Print ("Membership-form-response:\n" +
		           "    The Excel file from MS Forms, as a CSV file.\n")
		fmt.Print ("Prev-DB:\n" +
		           "    The previous database outputted by this program, from last meeting.\n")
		fmt.Print ("    If you omit this, a new member database will be created, just from the information in the form.\n")
 return
	default:
		fmt.Print("Problem: incorrect number of arguments.\n")
		os.Exit  (1)
	}
	
	/* CREATE OUTPUT FILE FOR NEXT MEMBER DATABASE */
	nextMemberDBCSV, err = os.Create(
		"Membership " +
		time.Now().Format(time.DateOnly) +
		".csv")
	if err != nil { panic(err) }
	defer nextMemberDBCSV.Close() // Note to non-GOphers: defer effectively pushes a function call onto a stack, and each of these calls will be popped off the stack and executed at the end of the current function.
	
	/*if justCreateMemberDB {
		/* CREATE NEW MEMBER DATABASE FROM FORM *//*
		if memberDB.applyNew(newFormCSV) == false {
			os.Exit(1)
		}
	} else {
		/* LOAD PREVIOUS MEMBER DATABASE *//*
		if memberDB.parse(prevMemberDBCSV) == false {
			os.Exit(1)
		}
		
		if memberDB.applyNew(newFormCSV) == false {
			os.Exit(1)
		}
	}*/
	
	if !justCreateMemberDB {
		/* LOAD PREVIOUS MEMBER DATABASE */
		if memberDB.parse(prevMemberDBCSV) == false {
			fmt.Println("Fatal: failed to process the previous membership database.")
			os.Exit    (1)
		}
	}
	
	/* CREATE NEW MEMBER DATABASE FROM FORM, AND GET NEXT MEETING PREFERENCES */
	nmp := memberDB.applyNew(newFormCSV)
	if nmp == nil {
		fmt.Println("Fatal: failed to process the attendance form.")
		os.Exit    (1)
	}
	
	fmt.Println()
	
	/* PERFORM EXTRA VETTING ON THE MEMBER DATABASE, AND PRINT NEXT MEETING PREFERENCES STATISTICS */
	memberDB.vetExtra  ()
	fmt.Println        ("\n" +
	                    "Next meeting preferences statistics:")
	nmp.printStatistics()
	
	/* EXPORT THE MEMBER DATABASE */
	memberDB.export(nextMemberDBCSV)
}
